obj-m := hid-multitouchhack.o

BUILD_KERNEL=$(shell uname -r)
KOBJ := /lib/modules/$(BUILD_KERNEL)/build

all:
	make -C $(KOBJ) M=$(PWD) modules

clean:
	make -C $(KOBJ) M=$(PWD) clean
